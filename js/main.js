$(document).ready(function () {
    setInterval(function () {
        var currentTime = moment();
        $("#clock").text(currentTime.format("HH:mm:ss"));
        $("#dayOfWeek").text(currentTime.format("dddd"));
        $("#date").text(currentTime.format("Do MMMM,"));
        $("#year").text(currentTime.format("YYYY"));
    }, 1000);
});